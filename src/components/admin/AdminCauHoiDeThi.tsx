import { useEffect, useState } from "react"

import { ArrayCauHoiDeThi, CauHoiDeThi } from "../../utils/types"
import apiCauhoidethi from "../../utils/api/cauhoidethi"

interface AdminCauHoiDeThiProps {
  limit: number
}

function AdminCauHoiDeThi(props: AdminCauHoiDeThiProps): JSX.Element {
  const [cauhoidethiAll, setCauhoidethiAll] = useState<ArrayCauHoiDeThi>(undefined)

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayCauHoiDeThi = await apiCauhoidethi.getAll(100, 0)
      if (data !== undefined)
      setCauhoidethiAll(data)
    }

    fetchData()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý câu hỏi đề thi</h1>
      </div>
      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">ID Câu hỏi</th>
              <th scope="col" className="px-6 py-3">ID Câu trả lời</th>
            </tr>
          </thead>

          <tbody>
            { cauhoidethiAll !== undefined
              ?
              (cauhoidethiAll.map((cauhoidethi: CauHoiDeThi, thutu: number) => (
                <tr
                  key={cauhoidethi.cauhoidethi_id}
                  className={
                    `${thutu % 2 === 0
                    ? 'even:bg-gray-50 even:dark:bg-gray-800'
                    : 'odd:bg-white odd:dark:bg-gray-900'}
                    border-b hover:bg-gray-100 hover:cursor-pointers`
                  }
                >
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{cauhoidethi.cauhoidethi_id}</td>
                  <td className="px-6 py-4">{cauhoidethi.cauhoi_id}</td>
                  <td className="px-6 py-4">{cauhoidethi.cautraloi_id}</td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={4}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

export default AdminCauHoiDeThi