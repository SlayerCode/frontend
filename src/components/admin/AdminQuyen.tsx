import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayQuyen, Quyen } from "../../utils/types"
import apiQuyen from "../../utils/api/quyen"

function AdminQuyen(): JSX.Element {
  const [quyenAll, setQuyenAll] = useState<ArrayQuyen>(undefined)

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayQuyen = await apiQuyen.getAll(10, 0)
      if (data !== undefined)
      setQuyenAll(data)
    }

    fetchData()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản Lý Quyền</h1>
        <button className="bg-gray-700 transition duration-300 ease-in-out transform 
          hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>Thêm</span>
        </button>
      </div>

      <table className="text-center w-full mt-6 mr-10 shadow-lg rounded-lg">
        <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
          <tr className="text-white">
            <th scope="col" className="px-6 py-3">STT</th>
            <th scope="col" className="px-6 py-3">ID</th>
            <th scope="col" className="px-6 py-3">Tên quyền</th>
            <th scope="col" className="px-6 py-3">Action</th>
          </tr>
        </thead>

        <tbody>
          { quyenAll !== undefined
            ?
            (quyenAll.map((quyen: Quyen, thutu: number) => (
              <tr key={quyen.quyen_id} className={`${thutu % 2 === 0 ? 'even:bg-gray-50 even:dark:bg-gray-800' : 'odd:bg-white odd:dark:bg-gray-900'} 
                                            border-b hover:bg-gray-100 hover:cursor-pointers`}>
                <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                <td className="px-6 py-4">{quyen.quyen_id}</td>
                <td className="px-6 py-4">{quyen.quyen_name}</td>
                <td className="px-6 py-4">
                <button className="bg-gray-700 transition duration-300 ease-in-out transform 
          hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2">
                  Edit
                </button>
                <button className="bg-gray-700 transition duration-300 ease-in-out transform 
          hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg">
                  Delete
                </button>
                </td>
              </tr>
            )))
            :
            (
              <tr>
                <td colSpan={4}>Không có dữ liệu. Vui lòng thử lại sau</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </>
  )
}

export default AdminQuyen