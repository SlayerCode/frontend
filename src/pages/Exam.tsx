import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import usePreventNotLogin from "../hooks/usePreventNotLogin";
import useSetPageChoose from "../hooks/useSetPageChoose";
import ExamHeroBanner from "../components/exam/ExamHeroBanner";
import apiDeThiMonHoc from "../utils/api/dethimonhoc";
import { ArrayDeThiMonHoc } from "../utils/types";

interface ExamProps {
  rerender: boolean;
  changeRerender: (newRerender: boolean) => void;
  pageChoose: string;
  changePageChoose: (newPageChoose: string) => void;
}

function Exam(props: ExamProps): JSX.Element {
  const [searchKeyword, setSearchKeyword] = useState("");
  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchKeyword(event.target.value);
  };
  const [exams, setExams] = useState<ArrayDeThiMonHoc>([]);

  useEffect(() => {
    async function fetchData() {
      const data = await apiDeThiMonHoc.getAll(50, 0);
      setExams(data);
    }
    fetchData();
  }, []);

  const filteredExams = exams?.filter((exam) =>
    exam.dethimonhoc_name.toLowerCase().includes(searchKeyword.toLowerCase())
  );

  usePreventNotLogin();
  useSetPageChoose({
    rerender: props.rerender,
    changeRerender: props.changeRerender,
    pageChoose: props.pageChoose,
    changePageChoose: props.changePageChoose,
  });

  return (
    <div className="flex justify-center items-center h-screen bg-gradient-to-r from-gray-900 via-purple-500 to-pink-500">
      <div className="w-full sm:w-3/4 lg:w-3/4">
        <ExamHeroBanner />
        <div className="flex flex-col md:flex-row items-center justify-between mb-2">
          <h2 className="text-2xl text-white font-bold mb-4 border-b-2 border-sky-400">
            Danh sách các bài thi
          </h2>
          <div className="relative">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <FontAwesomeIcon
                icon={faMagnifyingGlass}
                className="w-5 h-5 text-gray-400"
              />
            </div>
            <input
              type="search"
              name=""
              id=""
              value={searchKeyword}
              onChange={handleSearchChange}
              className="block w-72 mr-96 py-2 pl-10 border-2 border-gray-100 rounded-lg shadow-lg focus:ring-sky-400 focus:border-sky-400 sm:text-sm"
              placeholder="Tìm kiếm"
            />
          </div>
        </div>
        <table className="w-full bg-white shadow-md rounded-lg">
          <thead className="bg-gray-200 text-gray-700">
            <tr>
              <th className="py-2 px-4">STT</th>
              <th className="py-2 px-4">Mã môn học</th>
              <th className="py-2 px-4">Môn thi</th>
              <th className="py-2 px-4">Thời gian</th>
              <th className="py-2 px-4">Action</th>
            </tr>
          </thead>
          <tbody className="text-gray-600">
            {filteredExams?.map((exam, index) => (
              <tr
                key={exam.dethimonhoc_id}
                className="hover:bg-gray-400 
                                            hover:text-white transition-all ease-in-out "
              >
                <td className="py-2 px-4 text-center">{index + 1}</td>
                <td className="py-2 px-4 text-center">{exam.dethimonhoc_id}</td>
                <td className="py-2 px-4 text-center">
                  {exam.dethimonhoc_name}
                </td>
                <td className="py-2 px-4 text-center">
                  {exam.dethimonhoc_time} phút
                </td>
                <td className="py-2 px-4 flex justify-center">
                  <Link to={`/quiz/${exam.dethimonhoc_id}`}>
                    <button
                      className="bg-sky-400 hover:bg-sky-450 z-50 w-20 shadow-md text-white 
                      py-1 px-3 rounded-md transition duration-300 ease-in-out transform hover:scale-110"
                    >
                      Thi
                    </button>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Exam;
